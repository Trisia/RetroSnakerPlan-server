import edu.hznu.retroSnakerPlanServer.utils.ByteUtils;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

/**
 * create by Cliven on 2018-06-01 14:51
 */
public class NormalTest {
    @Test
    public void testByte(){
        Integer a = ByteUtils.byteToShortInteger(new Byte[]{0x01,0x00}, 0);
        System.out.println(a);
        a = ByteUtils.byteToInt(new Byte[]{0x01,0x00,0x00,0x00}, 0);
        System.out.println(a);
    }

    @Test
    public void testIp() {
        try {
            System.out.println("本机的IP = " + InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testRandom() {
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            System.out.println(random.nextInt(3));
        }
    }
}
