package edu.hznu.retroSnakerPlanServer;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * create by Cliven on 2018-06-03 14:13
 */
public class ThreadTest {
    private static final Logger log = LoggerFactory.getLogger(ThreadTest.class);

    public static void main(String[] args) {
        ThreadTest threadTest = new ThreadTest();
        threadTest.testFixedThread();
    }

    private void testFixedThread(){

        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            final int index = i;
            fixedThreadPool.execute(() -> {
                try {
                    log.info("thread run times: {}",index);
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            });
        }
    }

    private void testThreadCache(){

        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 10; i++) {
            final int index = i;
            singleThreadExecutor.execute(() -> {
                try {
                    log.info("thread run times: {}",index);
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    log.error("Thread error",e);
                }
            });
        }
    }
}
