package edu.hznu.retroSnakerPlanServer.entity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * create by Cliven on 2018-06-01 11:03
 */
public class FoodTest {
    @Test
    public void serial() {
        Byte[] EXP = new Byte[]{
                0x00,
                0x00,
                0x00
        };
        Food food = new Food()
                .setX(0)
                .setY(0)
                .setColorType(0);
        Byte[] bytes = food.serial();
        assertArrayEquals(EXP, bytes);
    }
}