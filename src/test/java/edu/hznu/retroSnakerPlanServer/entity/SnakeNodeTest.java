package edu.hznu.retroSnakerPlanServer.entity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * create by Cliven on 2018-06-01 11:03
 */
public class SnakeNodeTest {

    @Test
    public void serial() {
        Byte[] EXP = new Byte[]{
                0x46,
                0x46,
                0x0A,
                0x09,
                0x01
        };
        SnakeNode snakeNode = new SnakeNode()
                .setX(70)
                .setY(70)
                .setColorType(10)
                .setId(9)
                .setDirection(Direction.DOWN);
        assertArrayEquals(EXP, snakeNode.serial());
    }
}