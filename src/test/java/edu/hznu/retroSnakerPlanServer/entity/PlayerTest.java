package edu.hznu.retroSnakerPlanServer.entity;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * create by Cliven on 2018-06-01 18:38
 */
public class PlayerTest {

    @Test
    public void testCustruct(){
        SnakeNode head = new SnakeNode().setX(1).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node1 = new SnakeNode().setX(2).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node2 = new SnakeNode().setX(2).setY(2).setColorType(9).setId(11).setDirection(Direction.UP);
        List<SnakeNode> snake = new LinkedList<>();
        snake.add(head);
        snake.add(node1);
        snake.add(node2);
        Player player =new Player(snake);
        System.out.println(player);

    }

}