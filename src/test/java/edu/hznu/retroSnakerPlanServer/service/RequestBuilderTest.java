package edu.hznu.retroSnakerPlanServer.service;

import edu.hznu.retroSnakerPlanServer.dto.RequestMsg;
import edu.hznu.retroSnakerPlanServer.entity.Direction;
import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.ReqType;
import edu.hznu.retroSnakerPlanServer.entity.SnakeNode;
import edu.hznu.retroSnakerPlanServer.utils.RequestBuilder;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * create by Cliven on 2018-06-01 11:01
 */
public class RequestBuilderTest {

    @Test
    public void testDEAD() {
        LinkedList<Byte> linkedList = new LinkedList<>();

        SnakeNode head = new SnakeNode().setX(1).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node1 = new SnakeNode().setX(2).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node2 = new SnakeNode().setX(2).setY(2).setColorType(9).setId(11).setDirection(Direction.UP);
        List<SnakeNode> snake = new LinkedList<>();
        snake.add(head);
        snake.add(node1);
        snake.add(node2);
        List<Food> foods = new LinkedList<>();
        Food food1 = new Food().setX(1).setY(1).setColorType(9);
        Food food2 = new Food().setX(2).setY(1).setColorType(2);
        foods.add(food1);
        foods.add(food2);

        linkedList.addLast((byte) (ReqType.DEAD & 0xFF));
        linkedList.addLast((byte) (11 & 0xFF));
        linkedList.addLast((byte) (5 & 0xFF));
        linkedList.addLast((byte) ((3 * SnakeNode.SIZE) & 0xFF));
        linkedList.addAll(Arrays.asList(head.serial()));
        linkedList.addAll(Arrays.asList(node1.serial()));
        linkedList.addAll(Arrays.asList(node2.serial()));
        linkedList.addLast((byte) (2 * Food.SIZE));
        linkedList.addAll(Arrays.asList(food1.serial()));
        linkedList.addAll(Arrays.asList(food2.serial()));

        Byte[] testData = linkedList.toArray(new Byte[0]);

        RequestMsg EXP = new RequestMsg()
                .setReqType(ReqType.DEAD)
                .setId(11)
                .setKiller(5)
                .setPlayerInfo(snake)
                .setEats(foods);

        System.out.println("Test Data:");
        System.out.println(RequestBuilder.build(testData));
        System.out.println("EXP:");
        System.out.println(EXP.toString());

        assertEquals(EXP.toString(), RequestBuilder.build(testData).toString());
    }

    @Test
    public void testUPDATEFood() {
        LinkedList<Byte> linkedList = new LinkedList<>();

        SnakeNode head = new SnakeNode().setX(1).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node1 = new SnakeNode().setX(2).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node2 = new SnakeNode().setX(2).setY(2).setColorType(9).setId(11).setDirection(Direction.UP);
        List<SnakeNode> snake = new LinkedList<>();
        snake.add(head);
        snake.add(node1);
        snake.add(node2);
        List<Food> foods = new LinkedList<>();
        Food food1 = new Food().setX(1).setY(1).setColorType(9);
        Food food2 = new Food().setX(2).setY(1).setColorType(2);
        foods.add(food1);
        foods.add(food2);

        linkedList.addLast((byte) (ReqType.UPDATE & 0xFF));
        linkedList.addLast((byte) (11 & 0xFF));
        linkedList.addLast((byte) ((3 * SnakeNode.SIZE) & 0xFF));
        linkedList.addAll(Arrays.asList(head.serial()));
        linkedList.addAll(Arrays.asList(node1.serial()));
        linkedList.addAll(Arrays.asList(node2.serial()));
        linkedList.addLast((byte) (2 * Food.SIZE));
        linkedList.addAll(Arrays.asList(food1.serial()));
        linkedList.addAll(Arrays.asList(food2.serial()));

        Byte[] testData = linkedList.toArray(new Byte[0]);

        RequestMsg EXP = new RequestMsg()
                .setReqType(ReqType.UPDATE)
                .setId(11)
                .setEats(foods)
                .setPlayerInfo(snake);

        System.out.println("Test Data:");
        System.out.println(RequestBuilder.build(testData));
        System.out.println("EXP:");
        System.out.println(EXP.toString());

        assertEquals(EXP.toString(), RequestBuilder.build(testData).toString());
    }

    @Test
    public void testUPDATE() {
        LinkedList<Byte> linkedList = new LinkedList<>();

        SnakeNode head = new SnakeNode().setX(1).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node1 = new SnakeNode().setX(2).setY(1).setColorType(9).setId(11).setDirection(Direction.LEFT);
        SnakeNode node2 = new SnakeNode().setX(2).setY(2).setColorType(9).setId(11).setDirection(Direction.UP);
        List<SnakeNode> list = new LinkedList<>();
        list.add(head);
        list.add(node1);
        list.add(node2);

        linkedList.addLast((byte) (ReqType.UPDATE & 0xFF));
        linkedList.addLast((byte) (11 & 0xFF));
        linkedList.addLast((byte) ((3 * SnakeNode.SIZE) & 0xFF));
        linkedList.addAll(Arrays.asList(head.serial()));
        linkedList.addAll(Arrays.asList(node1.serial()));
        linkedList.addAll(Arrays.asList(node2.serial()));
        linkedList.addLast((byte) 0);

        Byte[] testData = linkedList.toArray(new Byte[0]);

        RequestMsg EXP = new RequestMsg()
                .setReqType(ReqType.UPDATE)
                .setId(11)
                .setEats(new LinkedList<>())
                .setPlayerInfo(list);

        System.out.println("Test Data:");
        System.out.println(RequestBuilder.build(testData));
        System.out.println("EXP:");
        System.out.println(EXP.toString());

        assertEquals(EXP.toString(), RequestBuilder.build(testData).toString());
    }

    @Test
    public void testNO_ID() {
        Byte[] noID = new Byte[]{0x00};
        RequestMsg requestMsg = RequestBuilder.build(noID);
        RequestMsg EXP = new RequestMsg().setReqType(ReqType.NO_ID);
        System.out.println(EXP);
        assertEquals(EXP.toString(), requestMsg.toString());
    }

}