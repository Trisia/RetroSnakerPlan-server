package edu.hznu.retroSnakerPlanServer.service;

import edu.hznu.retroSnakerPlanServer.dto.ResponseMsg;
import edu.hznu.retroSnakerPlanServer.entity.Direction;
import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.ReqType;
import edu.hznu.retroSnakerPlanServer.entity.SnakeNode;
import edu.hznu.retroSnakerPlanServer.utils.ResponseEncode;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * create by Cliven on 2018-06-01 12:20
 */
public class ResponseEncodeTest {

    @Test
    public void testUPDATE() {
        LinkedList<SnakeNode> snakeNode1 = new LinkedList<>();
        LinkedList<SnakeNode> snakeNode2 = new LinkedList<>();
        SnakeNode snakeNode10 = new SnakeNode().setX(1).setY(0).setColorType(1).setId(1).setDirection(Direction.LEFT);
        snakeNode1.add(snakeNode10);
        System.out.println(Arrays.toString(snakeNode10.serial()));

        SnakeNode snakeNode20 = new SnakeNode().setX(2).setY(3).setColorType(3).setId(2).setDirection(Direction.LEFT);
        SnakeNode snakeNode21 = new SnakeNode().setX(3).setY(3).setColorType(3).setId(2).setDirection(Direction.LEFT);
        snakeNode2.add(snakeNode20);
        snakeNode2.add(snakeNode21);
        System.out.println(Arrays.toString(snakeNode20.serial()));
        System.out.println(Arrays.toString(snakeNode21.serial()));
        List<List<SnakeNode>> playersInfo = new LinkedList<>();
        playersInfo.add(snakeNode1);
        playersInfo.add(snakeNode2);

        LinkedList<Food> foods = new LinkedList<>();
        Food food1 = new Food().setX(9).setY(9).setColorType(5);
        foods.add(food1);
        System.out.println(Arrays.toString(food1.serial()));

        ResponseMsg responseMsg = new ResponseMsg()
                .setResType(ReqType.UPDATE)
                .setId(1)
                .setScore(0)
                .setPlayersInfo(playersInfo)
                .setFoods(foods);


        Byte[] EXP = new Byte[]{
                (byte) (ReqType.UPDATE & 0xFF), 1, 0,
                (byte) (responseMsg.getPlayersInfoLen() & 0xFF),
                1, 0, 1, 1, 2,
                2, 3, 3, 2, 2,
                3, 3, 3, 2, 2,
                (byte) (responseMsg.getFoodsLen() & 0xFF),
                9, 9, 5
        };
        Byte[] data = ResponseEncode.encode(responseMsg);
        assertArrayEquals(EXP,data);

    }

    @Test
    public void testDEAD() {
        Byte[] EXP = new Byte[]{
                (byte) (ReqType.DEAD & 0xFF),
                (byte) (11 & 0xFF),
                (byte) 0
        };
        ResponseMsg responseMsg = new ResponseMsg()
                .setResType(ReqType.DEAD)
                .setId(11)
                .setScore(0);

        Byte[] res = ResponseEncode.encode(responseMsg);
        assertArrayEquals(EXP, res);
    }
}