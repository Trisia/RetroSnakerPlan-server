package edu.hznu.retroSnakerPlanServer;

import edu.hznu.retroSnakerPlanServer.controller.ProcessController;
import edu.hznu.retroSnakerPlanServer.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;

/**
 * create by Cliven on 2018-06-03 10:17
 */
public class UDPThreadTest {

    private static final Logger log = LoggerFactory.getLogger(UDPThreadTest.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        String responseMsg = "aaa";
        byte[] responseData = new byte[]{0x00, 0x01, 0x10, (byte) 127};
        byte[] buf = new byte[1024];
        // 监听3000端口
        DatagramSocket datagramSocket = new DatagramSocket(8086);
        // 打包收到消息的
        DatagramPacket receivedDatagramPacket = new DatagramPacket(buf, 1024);

        ProcessController processController = new ProcessController();
        log.info("Server up!");
        while (true) {

            // 从监听端口接收数据，该方法会导致线程阻塞
            datagramSocket.receive(receivedDatagramPacket);

            Thread temp = new Thread(() -> {
                log.info("Received msg:");
                StringBuilder receivedMsgBuilder = new StringBuilder();
                log.debug("{}", ByteUtils.bytesToHex(receivedDatagramPacket.getData(), 0, receivedDatagramPacket.getLength()));

                receivedMsgBuilder.append(new String(receivedDatagramPacket.getData(), 0, receivedDatagramPacket.getLength()))
                        .append(" message from -> ")
                        .append(receivedDatagramPacket.getAddress().getHostAddress())
                        .append(":")
                        .append(receivedDatagramPacket.getPort());

                log.info(receivedMsgBuilder.toString());
                try {
                    // 响应数据到客户端
                    log.info("response:{}",ByteUtils.bytesToHex(responseData, 0, responseData.length));
                    DatagramPacket responsePacket = new DatagramPacket(responseData,
                            responseData.length,
                            receivedDatagramPacket.getAddress(),
                            receivedDatagramPacket.getPort());
                    datagramSocket.send(responsePacket);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //由于receivedDatagramPacket在接收了数据之后，其内部消息长度值会变为实际接收的消息的字节数，
                //所以这里要将dp_receive的内部消息长度重新置为1024
//                receivedDatagramPacket.setLength(1024);
            });
            temp.run();
//            log.info("Received msg:");
//            StringBuilder receivedMsgBuilder = new StringBuilder();
//            receivedMsgBuilder.append(new String(receivedDatagramPacket.getData(), 0, receivedDatagramPacket.getLength()))
//                    .append(" message from -> ")
//                    .append(receivedDatagramPacket.getAddress().getHostAddress())
//                    .append(":")
//                    .append(receivedDatagramPacket.getPort());
//
//            log.info(">>>> " + receivedMsgBuilder.toString());
//            log.info(">>>> Sleep...");
//            Thread.sleep(10000);
//            log.info(">>>> Wake up...");
//            // 响应数据到客户端的3000端口
//            DatagramPacket responsePacket = new DatagramPacket(responseMsg.getBytes(StandardCharsets.US_ASCII),
//                    responseMsg.length(),
//                    receivedDatagramPacket.getAddress(),
//                    receivedDatagramPacket.getPort());
//
//            datagramSocket.send(responsePacket);
//            //由于receivedDatagramPacket在接收了数据之后，其内部消息长度值会变为实际接收的消息的字节数，
//            //所以这里要将dp_receive的内部消息长度重新置为1024
//            receivedDatagramPacket.setLength(1024);
        }
    }
}
