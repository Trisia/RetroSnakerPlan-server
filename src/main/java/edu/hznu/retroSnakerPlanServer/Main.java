package edu.hznu.retroSnakerPlanServer;

import edu.hznu.retroSnakerPlanServer.worker.UDPWorker;

/**
 * create by Cliven on 2018-05-31 15:49
 *
 * @author Cliven
 * 服务器启动类
 */
public class Main {

    public static void main(String[] args) {
        UDPWorker udpWorker = new UDPWorker(8086);
        udpWorker.run();
    }
}
