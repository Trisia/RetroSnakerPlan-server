package edu.hznu.retroSnakerPlanServer.entity;

import java.util.Collections;
import java.util.List;

/**
 * create by Cliven on 2018-06-01 13:44
 *
 * @author Cliven
 * 玩家信息
 */
public class Player {
    private Integer id;
    private List<SnakeNode> snakeNodes;
    private Integer score;
    private Area area;
    private Integer msgCnt;
    private Long lastUpdateTime;

    public Player(List<SnakeNode> snakeNodes) {
        if (snakeNodes == null || snakeNodes.size() == 0) {
            throw new IllegalArgumentException("SnakeNode is empty!");
        }
        this.id = snakeNodes.get(0).getId();
        this.score = 0;
        this.setSnakeNodes(snakeNodes);
        this.lastUpdateTime = System.currentTimeMillis();
    }

    public Player(List<SnakeNode> snakeNodes, Integer score) {
        this(snakeNodes);
        this.score = score;
        this.msgCnt = 0;
    }

    public Integer getMsgCnt() {
        return msgCnt;
    }

    public void setMsgCnt(Integer msgCnt) {
        this.msgCnt = msgCnt;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public Player setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
        return this;
    }

    /**
     * 增加分数
     */
    public void addScore() {
        this.score++;
    }

    public Area getArea() {
        return area;
    }

    public Player setArea(Area area) {
        this.area = area;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Player setId(Integer id) {
        this.id = id;
        return this;
    }

    public List<SnakeNode> getSnakeNodes() {
        return snakeNodes;
    }

    /**
     * set的同时创建区域
     *
     * @param snakeNodes 节点数据
     */
    public Player setSnakeNodes(List<SnakeNode> snakeNodes) {
        Integer sx = Integer.MAX_VALUE;
        Integer sy = Integer.MAX_VALUE;
        Integer ex = Integer.MIN_VALUE;
        Integer ey = Integer.MIN_VALUE;
        Integer x, y;
        for (SnakeNode node : snakeNodes) {
            x = node.getX();
            y = node.getY();
            if (x < sx) {
                sx = x;
            }
            if (x > ex) {
                ex = x;
            }

            if (y < sy) {
                sy = y;
            }
            if (y > ey) {
                ey = y;
            }
        }
        this.snakeNodes = Collections.synchronizedList(snakeNodes);
        return setArea(new Area(sx, sy, ex, ey));
    }

    public Integer getScore() {
        return score;
    }

    public Player setScore(Integer score) {
        this.score = score;
        return this;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", snakeNodes=" + snakeNodes +
                ", score=" + score +
                ", area=" + area +
                ", msgCnt=" + msgCnt +
                '}';
    }
}
