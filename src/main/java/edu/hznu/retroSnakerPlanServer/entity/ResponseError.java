package edu.hznu.retroSnakerPlanServer.entity;

import edu.hznu.retroSnakerPlanServer.service.ByteSerializable;

import java.nio.charset.StandardCharsets;

/**
 * create by Cliven on 2018-06-01 15:47
 */
public class ResponseError implements ByteSerializable {
    private static final Integer RESTYPE = 3;
    private String info;


    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * 序列化某个对象为byte数组
     *
     * @return 字节数组
     * @author Cliven
     * @since 2018-6-1 15:54:29
     */
    @Override
    public Byte[] serial() {
        byte resType = (byte) (RESTYPE & 0xFF);
        byte[] infoByte = this.info.getBytes(StandardCharsets.US_ASCII);
        int offset = 0;

        Byte[] res = new Byte[infoByte.length + 1];
        res[offset++] = resType;
        for (Byte b : infoByte) {
            res[offset++] = b;
        }
        return res;
    }
}
