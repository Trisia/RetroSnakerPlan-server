package edu.hznu.retroSnakerPlanServer.entity;

/**
 * create by Cliven on 2018-06-01 16:49
 *
 * @author Cliven
 * 区域
 */
public class Area {
    public Integer sx;
    public Integer sy;
    public Integer ex;
    public Integer ey;

    public Area() {
    }

    public Area(Integer sx, Integer sy, Integer ex, Integer ey) {
        this.sx = sx;
        this.sy = sy;
        this.ex = ex;
        this.ey = ey;
    }



    /**
     * 判断某个点是否在区域内
     *
     * @param x
     * @param y
     * @return 在/不在
     * @author Cliven
     * @since 2018-06-01 20:04:33
     */
   public boolean isInArea(Integer x, Integer y) {
        return (x >= sx && x <= ex) && (y >= sy && y <= ey);
    }

    @Override
    public String toString() {
        return "Area{" +
                "sx=" + sx +
                ", sy=" + sy +
                ", ex=" + ex +
                ", ey=" + ey +
                '}';
    }
}
