package edu.hznu.retroSnakerPlanServer.entity;

import edu.hznu.retroSnakerPlanServer.exception.UnknownMsgTypeException;

/**
 * create by Cliven on 2018-05-31 16:25
 *
 * @author Cliven
 * 请求类型
 */
public class ReqType {
    public static Integer NO_ID = 255;
    public static Integer UPDATE = 1;
    public static Integer DEAD = 2;

    public static void check(Integer type) {
        switch (type) {
            case 255:
            case 1:
            case 2:
                break;
            default:
                throw new UnknownMsgTypeException("unknown request type!");
        }
    }
}
