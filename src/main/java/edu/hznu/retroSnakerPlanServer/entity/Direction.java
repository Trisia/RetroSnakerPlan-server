package edu.hznu.retroSnakerPlanServer.entity;

/**
 * create by Cliven on 2018-05-31 16:26
 */
public class Direction {
    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
}
