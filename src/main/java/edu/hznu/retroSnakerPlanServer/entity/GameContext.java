package edu.hznu.retroSnakerPlanServer.entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * create by Cliven on 2018-06-01 13:43
 *
 * @author Cliven
 * 游戏环境，用于存储玩家信息和
 */
public class GameContext {

    private static final Integer MAX_PLAYER_NUMBER_DEFAULT = 50;
    private static final Integer WIDTH_DEFAULT = 100;
    private static final Integer HEIGHT_DEFAULT = 100;
    private static final Integer MAX_FOOD_NUMBER_DEFAULT = (HEIGHT_DEFAULT * WIDTH_DEFAULT) * 5 / 100;
    private static final Integer INIT_LENGTH_DEFAULT = 5;
    private static final Integer SEARCH_RADIUSS_DEFAULT = 20;
    /**
     * 玩家超时时间
     */
    private static final Long PLAYER_CONNECTION_TIME_OUT_SEC = 5L;

    private Map<Integer, Player> players;
    private Food[][] foods;
    private Integer width;
    private Integer height;
    private Integer maxPlayerNumber;
    private Integer maxFoodNumber;
    private Integer initLength;
    private Integer foodCnt;
    private Integer searchRadius;
    private Long playerConnectionTimeOutMillsec;


    public GameContext() {
        this.width = WIDTH_DEFAULT;
        this.height = HEIGHT_DEFAULT;
        this.maxPlayerNumber = MAX_PLAYER_NUMBER_DEFAULT;
        this.maxFoodNumber = MAX_FOOD_NUMBER_DEFAULT;
        this.initLength = INIT_LENGTH_DEFAULT;
        this.foods = new Food[this.width + 2][this.height + 2];
        this.players = Collections.synchronizedMap(new HashMap<>(this.maxPlayerNumber));
        this.foodCnt = 0;
        this.searchRadius = SEARCH_RADIUSS_DEFAULT;
        this.playerConnectionTimeOutMillsec = PLAYER_CONNECTION_TIME_OUT_SEC * 1000;
    }

//    public GameContext(Integer width, Integer height, Integer initLength, Integer maxPlayerNumber, Integer maxFoodNumber) {
//        this.width = width;
//        this.height = height;
//        this.maxPlayerNumber = maxPlayerNumber;
//        this.maxFoodNumber = maxFoodNumber;
//        this.foods = new Food[this.width + 2][this.height + 2];
//        this.players = Collections.synchronizedMap(new HashMap<>(this.maxPlayerNumber));
//        this.initLength = initLength;
//        this.foodCnt = 0;
//        this.searchRadius = SEARCH_RADIUSS_DEFAULT;
//    }


    public Long getPlayerConnectionTimeOutMillsec() {
        return playerConnectionTimeOutMillsec;
    }

    public void setPlayerConnectionTimeOutMillsec(Long playerConnectionTimeOutMillsec) {
        this.playerConnectionTimeOutMillsec = playerConnectionTimeOutMillsec;
    }

    public void addFoodCnt() {
        this.foodCnt++;
    }

    public void reduceFoodCnt() {
        this.foodCnt--;
    }

    public Integer getSearchRadius() {
        return searchRadius;
    }

    public void setSearchRadius(Integer searchRadius) {
        this.searchRadius = searchRadius;
    }

    public Integer getFoodCnt() {
        return foodCnt;
    }

    public void setFoodCnt(Integer foodCnt) {
        this.foodCnt = foodCnt;
    }

    public Map<Integer, Player> getPlayers() {
        return players;
    }

    public void setPlayers(Map<Integer, Player> players) {
        this.players = players;
    }

    public Food[][] getFoods() {
        return foods;
    }

    public void setFoods(Food[][] foods) {
        this.foods = foods;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getMaxPlayerNumber() {
        return maxPlayerNumber;
    }

    public void setMaxPlayerNumber(Integer maxPlayerNumber) {
        this.maxPlayerNumber = maxPlayerNumber;
    }

    public Integer getMaxFoodNumber() {
        return maxFoodNumber;
    }

    public void setMaxFoodNumber(Integer maxFoodNumber) {
        this.maxFoodNumber = maxFoodNumber;
    }

    public Integer getInitLength() {
        return initLength;
    }

    public void setInitLength(Integer initLength) {
        this.initLength = initLength;
    }
}
