package edu.hznu.retroSnakerPlanServer.factory;

import edu.hznu.retroSnakerPlanServer.controller.ProcessController;

/**
 * create by Cliven on 2018-06-03 11:01
 *
 * @author Cliven
 * 工厂对象用于获取单例的
 */
public class ProcessControllerFactory {
    private volatile static ProcessController instance = null;

    private ProcessControllerFactory() {
    }

    public static ProcessController getInstance() {
        if (instance == null) {
            synchronized (ProcessControllerFactory.class) {
                if (instance == null) {
                    instance = new ProcessController();
                }
            }
        }
        return instance;
    }
}
