package edu.hznu.retroSnakerPlanServer.factory;

import edu.hznu.retroSnakerPlanServer.entity.GameContext;

/**
 * create by Cliven on 2018-06-01 19:11
 *
 * @author Cliven
 * 游戏环境工厂
 */
public class GameContextFactory {

    private volatile static GameContext context = null;

    public static GameContext generate() {
        if (context == null) {
            synchronized (GameContext.class) {
                if (context == null) {
                    context = new GameContext();
                }
            }
        }
        return context;
    }
}
