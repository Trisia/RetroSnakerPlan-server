package edu.hznu.retroSnakerPlanServer.dto;

import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.SnakeNode;

import java.util.List;

/**
 * create by Cliven on 2018-05-31 16:13
 *
 * @author Cliven
 * 请求数据包数据格式
 */
public class RequestMsg {

    private Integer reqType;
    private Integer msgCnt;
    private Integer id;
    private Integer killer;
    private Integer playerInfoLen;
    private List<SnakeNode> playerInfo;
    private Integer eatsLen;
    private List<Food> eats;


    public Integer getMsgCnt() {
        return msgCnt;
    }

    public RequestMsg setMsgCnt(Integer msgCnt) {
        this.msgCnt = msgCnt;return this;
    }

    public Integer getPlayerInfoLen() {
        return playerInfoLen;
    }

    private RequestMsg setPlayerInfoLen(Integer playerInfoLen) {
        this.playerInfoLen = playerInfoLen;
        return this;
    }

    public Integer getEatsLen() {
        return eatsLen;
    }

    private RequestMsg setEatsLen(Integer eatsLen) {
        this.eatsLen = eatsLen;
        return this;
    }

    public Integer getReqType() {
        return reqType;
    }

    public RequestMsg setReqType(Integer reqType) {
        this.reqType = reqType;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public RequestMsg setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getKiller() {
        return killer;
    }

    public RequestMsg setKiller(Integer killer) {
        this.killer = killer;
        return this;
    }

    public List<SnakeNode> getPlayerInfo() {
        return playerInfo;
    }

    public RequestMsg setPlayerInfo(List<SnakeNode> playerInfo) {
        this.playerInfo = playerInfo;
        return setPlayerInfoLen(SnakeNode.SIZE * playerInfo.size());
    }

    public List<Food> getEats() {
        return eats;
    }

    public RequestMsg setEats(List<Food> eats) {
        this.eats = eats;
        return setEatsLen(eats.size() * Food.SIZE);
    }

    @Override
    public String toString() {
        return "RequestMsg{" +
                "reqType=" + reqType +
                ", msgCnt=" + msgCnt +
                ", id=" + id +
                ", killer=" + killer +
                ", playerInfoLen=" + playerInfoLen +
                ", playerInfo=" + playerInfo +
                ", eatsLen=" + eatsLen +
                ", eats=" + eats +
                '}';
    }
}
