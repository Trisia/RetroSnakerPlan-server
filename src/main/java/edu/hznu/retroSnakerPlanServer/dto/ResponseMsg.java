package edu.hznu.retroSnakerPlanServer.dto;

import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.SnakeNode;

import java.util.List;

/**
 * create by Cliven on 2018-05-31 16:19
 *
 * @author Cliven
 * 响应客户端消息数据结构
 */
public class ResponseMsg {

    private Integer resType;
    private Integer msgCnt = 0;
    private Integer id;
    private Integer score = 0;
    private Integer playersInfoLen;
    private List<List<SnakeNode>> playersInfo;
    private Integer foodsLen;
    private List<Food> foods;

    public Integer getMsgCnt() {
        return msgCnt;
    }

    public ResponseMsg setMsgCnt(Integer msgCnt) {
        this.msgCnt = msgCnt; return this;
    }

    public Integer getResType() {
        return resType;
    }

    public ResponseMsg setResType(Integer resType) {
        this.resType = resType;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public ResponseMsg setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getScore() {
        return score;
    }

    public ResponseMsg setScore(Integer score) {
        this.score = score;
        return this;
    }

    public Integer getPlayersInfoLen() {
        return playersInfoLen;
    }

    private ResponseMsg setPlayersInfoLen(Integer playersInfoLen) {
        this.playersInfoLen = playersInfoLen;
        return this;
    }

    public List<List<SnakeNode>> getPlayersInfo() {
        return playersInfo;
    }

    public ResponseMsg setPlayersInfo(List<List<SnakeNode>> playersInfo) {
        Integer len = 0;
        for (List<SnakeNode> snake : playersInfo) {
            len += (snake.size() * SnakeNode.SIZE);
        }
        this.playersInfo = playersInfo;
        return setPlayersInfoLen(len);
    }

    public Integer getFoodsLen() {
        return foodsLen;
    }

    private ResponseMsg setFoodsLen(Integer foodsLen) {
        this.foodsLen = foodsLen;
        return this;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public ResponseMsg setFoods(List<Food> foods) {
        this.foods = foods;
        return setFoodsLen(foods.size() * Food.SIZE);
    }

    @Override
    public String toString() {
        return "ResponseMsg{" +
                "resType=" + resType +
                ", msgCnt=" + msgCnt +
                ", id=" + id +
                ", score=" + score +
                ", playersInfoLen=" + playersInfoLen +
                ", playersInfo=" + playersInfo +
                ", foodsLen=" + foodsLen +
                ", foods=" + foods +
                '}';
    }
}
