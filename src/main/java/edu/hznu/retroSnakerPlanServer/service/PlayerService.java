package edu.hznu.retroSnakerPlanServer.service;

import edu.hznu.retroSnakerPlanServer.entity.Area;
import edu.hznu.retroSnakerPlanServer.entity.Player;

import java.util.List;

/**
 * create by Cliven on 2018-06-01 14:02
 *
 * @author Cliven
 * 地图服务类，用于操作地图内容
 */
public interface PlayerService {

    /**
     * 获取区域内的所有玩家
     * @param area 区域
     * @return 区域内的所有玩家
     * @author Cliven
     * @since 2018-06-01 20:01:15
     */
    List<Player> getAreaPlayer(Area area);

    /**
     * 创建玩家
     *
     * @return 新玩家信息
     * @author Cliven
     * @since 2018-06-01 14:06:24
     */
    Player create();

    /**
     * 新增玩家
     * @param player 玩家
     * @return 加入的玩家
     * @author Cliven
     * @since 2018-06-04 11:14:05
     */
    Player add(Player player);

    /**
     * 移除玩家
     *
     * @param id 玩家ID
     * @return 被移除的玩家信息
     * @author Cliven
     * @since 2018-06-01 14:07:31
     */
    Player remove(Integer id);

    /**
     * 更新玩家信息
     *
     * @param player 要更新的玩家信息
     * @return 更新后的玩家信息
     * @author Cliven
     * @since 2018-06-01 14:09:07
     */
    Player update(Player player);

    /**
     * 查找玩家信息
     *
     * @param id 玩家ID
     * @return 空/玩家信息
     * @author Cliven
     * @since 2018-06-01 14:11:22
     */
    Player findById(Integer id);
}
