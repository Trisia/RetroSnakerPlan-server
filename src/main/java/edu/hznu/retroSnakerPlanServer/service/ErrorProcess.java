package edu.hznu.retroSnakerPlanServer.service;

import java.nio.charset.StandardCharsets;

/**
 * create by Cliven on 2018-06-03 11:28
 *
 * @author Cliven
 * 用捕获到的异常创建失败消息字节串
 */
public class ErrorProcess {

    /**
     * 处理异常构造为响应消息的实体，然后序列化
     *
     * @param e 异常
     * @return 消息字节串
     * @author Cliven
     * @since 2018-06-03 13:55:14
     */
    public static byte[] process(Exception e) {
        String msg = e.getMessage() == null ? "NULL point exception" : e.getMessage();
        byte[] info = msg.getBytes(StandardCharsets.US_ASCII);
        byte resType = 0x03 & 0xFF;
        int len = info.length + 1;
        byte[] resp = new byte[len];
        for (int i = 0; i < len; i++) {
            if (i == 0) {
                resp[i] = resType;
            } else {
                resp[i] = info[i - 1];
            }
        }
        return resp;
    }
}
