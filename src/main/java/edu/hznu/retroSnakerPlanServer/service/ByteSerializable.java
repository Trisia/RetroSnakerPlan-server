package edu.hznu.retroSnakerPlanServer.service;

/**
 * create by Cliven on 2018-06-01 8:05
 *
 * @author Cliven
 * 用于实体序列化为byte数组
 */
public interface ByteSerializable{

    /**
     * 序列化某个对象为byte数组
     * @return 字节数组
     * @author Cliven
     * @since 2018-06-01 08:06:53
     */
    Byte[] serial();
}
