package edu.hznu.retroSnakerPlanServer.service;

import edu.hznu.retroSnakerPlanServer.entity.Area;
import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.SnakeNode;

import java.util.List;

/**
 * create by Cliven on 2018-06-01 14:12
 *
 * @author Cliven
 * 用于食物的服务
 */
public interface FoodService {

    /**
     * 批量删除游戏中的食物
     *
     * @param foods 需要除去的食物
     * @author Cliven
     * @since 2018-06-01 20:53:39
     */
    void removeFoods(List<Food> foods);

    /**
     * 通过区域信息获取区域内的食物
     *
     * @param area 区域
     * @return 区域内的所有食物
     * @author Cliven
     * @since 2018-06-01 20:26:59
     */
    List<Food> getFoodsByArea(Area area);

    /**
     * 随机生成一个食物
     *
     * @return 生成的食物信息
     * @author Cliven
     * @since 2018-06-01 14:13:34
     */
    Food create();

    /**
     * 把整条蛇作为食物
     *
     * @param snake 蛇的节点
     * @return 生成食物信息
     * @author Cliven
     * @since 2018-06-01 16:19:30
     */
    List<Food> add(List<SnakeNode> snake);

    /**
     * 根据蛇节点生成食物
     *
     * @param node 蛇的节点
     * @return 生成食物信息
     * @author Cliven
     * @since 2018-06-01 16:19:06
     */
    Food add(SnakeNode node);

    /**
     * 删除一个食物
     *
     * @param x 坐标x
     * @param y 坐标y
     * @return 被删除的食物信息
     * @author Cliven
     * @since 2018-06-01 14:15:42
     */
    Food remove(Integer x, Integer y);

    /**
     * 查找食物
     *
     * @param x 坐标x
     * @param y 坐标y
     * @return 食物信息
     * @author Cliven
     * @since 2018-06-01 14:16:41
     */
    Food find(Integer x, Integer y);

    /**
     * 更新食物信息
     *
     * @param food 修改原先的食物信息
     * @return 食物信息
     * @author Cliven
     * @since 2018-06-01 14:17:19
     */
    Food update(Food food);
}
