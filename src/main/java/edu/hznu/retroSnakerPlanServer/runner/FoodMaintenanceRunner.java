package edu.hznu.retroSnakerPlanServer.runner;

import edu.hznu.retroSnakerPlanServer.entity.Food;
import edu.hznu.retroSnakerPlanServer.entity.GameContext;
import edu.hznu.retroSnakerPlanServer.factory.GameContextFactory;
import edu.hznu.retroSnakerPlanServer.service.FoodService;
import edu.hznu.retroSnakerPlanServer.service.imp.FoodServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * create by Cliven on 2018-06-04 8:14
 *
 * @author Cliven
 * 游戏中食物信息的维护者
 */
public class FoodMaintenanceRunner implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(FoodMaintenanceRunner.class);
    private FoodService foodService;

    public FoodMaintenanceRunner() {
        GameContext ctx = GameContextFactory.generate();
        foodService = new FoodServiceImpl(ctx);
    }

    @Override
    public void run() {
        // 不间断的维护食物信息
        log.info("Init map food information...");
        while (true) {
            try {
                // Create 方法生效，当地图上食物数量少于固定值时
                foodService.create();
            } catch (RuntimeException e) {
                log.error("Food Maintenance thread get an error:", e);
            }
        }
    }
}
