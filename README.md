# RetroSnakerPlan-server

#### 项目介绍

STM32F1贪吃蛇大作战服务端，该服务将在8086端口上监听。


#### 安装教程

##### 如何安装运行服务端程序

1. 下载编译好的代码[下载页面](https://gitee.com/Trisia/RetroSnakerPlan-server/releases)
2. 安装JRE 运行环境。
3. 关闭在8086端口上运行的服务。
4. 使用`java -jar RetroSnakerPlan-server.jar`命令就可以运行服务端程序。（需要先先切换目录到`RetroSnakerPlan-server.jar所在目录`）

